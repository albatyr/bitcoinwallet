﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Coinbase.Client.Models;
using Coinbase.Client.Web;
using RestSharp;

namespace Coinbase.Client
{
    public class CoinbaseApiClient : ICoinbaseApiClient
    {
        private const string BaseUrl = "https://api.coinbase.com";
        private const string ApiKey = "kBP7dUfDgB3NFG2d";
        private const string ApiSecret = "tZeYSJ30FBF3VLa2gwBuPcpRPWm24xgT";
        
        private readonly IWebApiClient _httpClient;

        public CoinbaseApiClient(IWebApiClient httpClient)
        {
            _httpClient = httpClient;
        }
        
        public async Task<GetPaymentMethodsResponse> GetPaymentMethods()
        {
            const string requestPath = "v2/payment-methods";
            
            var headers = await GetHeaders(Method.GET, requestPath);

            return await _httpClient.Get<GetPaymentMethodsResponse>(BaseUrl, requestPath, headers);

        }

        public async Task<GetTimeResponse> GetTime()
        {
            return await _httpClient.Get<GetTimeResponse>(BaseUrl, "v2/time");
        }

        private async Task<IDictionary<string, string>> GetHeaders(Method method, string requestPath, string body = null)
        {
            var time = await GetTime();

            var signature = ApiKeyAuthenticator.GenerateSignature(
                time.Data.Epoch.ToString(),
                method.ToString(),
                "/" + requestPath,
                body,
                ApiSecret
            );

            return new Dictionary<string, string>
            {
                {"CB-ACCESS-KEY", ApiKey},
                {"CB-ACCESS-SIGN", signature},
                {"CB-ACCESS-TIMESTAMP", time.Data.Epoch.ToString()},
                {"content-type", "application/json"}
            };
        }
    }
    
}