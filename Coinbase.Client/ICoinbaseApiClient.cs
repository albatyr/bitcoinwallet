﻿using System.Threading.Tasks;
using Coinbase.Client.Models;

namespace Coinbase.Client
{
    public interface ICoinbaseApiClient
    {
        public Task<GetPaymentMethodsResponse> GetPaymentMethods();
        public Task<GetTimeResponse> GetTime();
    }
}