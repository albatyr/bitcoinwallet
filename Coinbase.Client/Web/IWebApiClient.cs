using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Coinbase.Client.Web
{
    public interface IWebApiClient
    {
        Task<T> Post<T>(string baseUrl, string resource, object body, 
            IDictionary<string, string> headers = null, 
            X509Certificate2Collection certificates = null);

        Task<T> Get<T>(string baseUrl, string resource = null, 
            IDictionary<string, string> headers = null, 
            X509Certificate2Collection certificates = null);
        
        Task<T> Delete<T>(string baseUrl, string resource = null, object body = null, 
            IDictionary<string, string> headers = null, 
            X509Certificate2Collection certificates = null);
        
        Task<T> Put<T>(string baseUrl, string resource = null, object body = null, 
            IDictionary<string, string> headers = null, 
            X509Certificate2Collection certificates = null);
    }
}