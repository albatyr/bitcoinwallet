﻿namespace Coinbase.Client.Web
{
    public interface ILog
    {
        public void Debug(string message);
    }
}