using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Xml.Linq;
using RestSharp;

namespace Coinbase.Client.Web
{
    public sealed class WebApiClient : IWebApiClient
    {
        private const string DefaultContentType = "application/json";
        
        private readonly Lazy<ILog> _log;
        private readonly JsonSerializerOptions _serializerSettings;

        public WebApiClient(Lazy<ILog> log)
        {
            _log = log;
            
            _serializerSettings = new JsonSerializerOptions();
            _serializerSettings.PropertyNameCaseInsensitive = true;
            _serializerSettings.IgnoreNullValues = true;
            _serializerSettings.Converters.Add(new JsonStringEnumConverter());
        }

        public async Task<T> Post<T>(string baseUrl, string resource, object body, 
            IDictionary<string, string> headers = null, 
            X509Certificate2Collection certificates = null) 
        {
            return await CallApi<T>(baseUrl, resource, Method.POST, body, headers, certificates);
        }

        public async Task<T> Get<T>(string baseUrl, string resource = null, 
            IDictionary<string, string> headers = null, 
            X509Certificate2Collection certificates = null) 
        {
            return await CallApi<T>(baseUrl, resource, Method.GET, null, headers, certificates);
        }

        public async Task<T> Delete<T>(string baseUrl, string resource = null, object body = null, 
            IDictionary<string, string> headers = null, 
            X509Certificate2Collection certificates = null) 
        {
            return await CallApi<T>(baseUrl, resource, Method.DELETE, body, headers, certificates);
        }

        public async Task<T> Put<T>(string baseUrl, string resource = null, object body = null, 
            IDictionary<string, string> headers = null, 
            X509Certificate2Collection certificates = null)  
        {
            return await CallApi<T>(baseUrl, resource, Method.PUT, body, headers, certificates);
        }
        
#pragma warning disable 618
        private static object GetRequestBody(IEnumerable<Parameter> parameters)
#pragma warning restore 618
        {
            var bodyParameter = parameters.FirstOrDefault(_ => _.Type == ParameterType.RequestBody);

            return bodyParameter?.Value;
        }
        
#pragma warning disable 618
        private static string GetRequestHeaders(IEnumerable<Parameter> parameters)
#pragma warning restore 618
        {
            return string.Join("\n", parameters
                .Where(_ => _.Type == ParameterType.HttpHeader)
                .Select(h => $"{h.Name}: {h.Value}"));
        }

        private static string GetBodyContentType(IDictionary<string, string> headers)
        {
            const string contentTypeKey = "content-type";
            
            if (headers == null)
                return DefaultContentType;
            
            if (!headers.ContainsKey(contentTypeKey))
                return DefaultContentType;
            
            var contentType = headers[contentTypeKey];
            return string.IsNullOrEmpty(contentType) ? DefaultContentType : contentType;
        }

        private async Task<T> CallApi<T>(
            string baseUrl, 
            string resource,
            Method httpMethod,
            object body = null,
            IDictionary<string, string> headers = null,
            X509Certificate2Collection certificates = null)
        {
            if (string.IsNullOrEmpty(baseUrl))
                throw new ArgumentNullException(nameof(baseUrl));

            var request = new RestRequest(resource ?? string.Empty, httpMethod) {RequestFormat = DataFormat.Json};

            if (headers != null)
            {
                foreach (var (key, value) in headers)
                {
                    request.AddHeader(key, value);
                }
            }

            if (body != null)
            {
                var contentType = GetBodyContentType(headers);
                var serializedBody = string.Empty;
                
                if (contentType.Equals(DefaultContentType, StringComparison.CurrentCultureIgnoreCase))
                {
                    serializedBody = JsonSerializer.Serialize(body, _serializerSettings);
                }
                else if (body is IDictionary<string, object> dictionaryBody)
                {
                    serializedBody = string.Join("&", dictionaryBody.Select(p =>
                        $"{p.Key}={JsonSerializer.Serialize(p.Value, _serializerSettings)}"));
                }

                if (!string.IsNullOrEmpty(serializedBody))
                {
                    request.AddParameter(contentType, serializedBody, ParameterType.RequestBody);
                }
            }
            
            var сlient = new RestClient(baseUrl);
            
            if (certificates != null)
            {
                сlient.ClientCertificates = certificates;
            }

            _log.Value.Debug($"API call [{httpMethod}] {сlient.BaseUrl?.AbsoluteUri}{resource}");
            
            var headersToLog = GetRequestHeaders(request.Parameters);
            if (!string.IsNullOrEmpty(headersToLog))
                _log.Value.Debug($"API call headers: \n{headersToLog}");

            var bodyToLog = GetRequestBody(request.Parameters);
            if (bodyToLog != null)
                _log.Value.Debug($"API body: \n{bodyToLog}");
            
            var sw = Stopwatch.StartNew();

            var response = await сlient.ExecuteAsync(request);

            sw.Stop();
            
            _log.Value.Debug($"API response [{response.StatusCode}]: \n{response.Content}");

            if (!response.IsSuccessful) 
                throw new Exception($"Status code: {response.StatusCode}, Content: {response.Content}");
            
            var responseType = typeof(T);
                
            if (responseType == typeof(object))
                return default;

            if (responseType == typeof(XElement))
                return (T)(object)XElement.Parse(response.Content);
                
            return JsonSerializer.Deserialize<T>(response.Content, _serializerSettings);
        }
    }
}