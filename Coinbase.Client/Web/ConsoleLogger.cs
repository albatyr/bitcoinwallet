﻿using System;

namespace Coinbase.Client.Web
{
    public class ConsoleLogger : ILog
    {
        public void Debug(string message)
        {
            Console.WriteLine(message);
        }
    }
}