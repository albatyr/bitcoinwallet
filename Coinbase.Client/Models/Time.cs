﻿using System;
using System.Text.Json.Serialization;

namespace Coinbase.Client.Models
{
    public class Time
    {
        [JsonPropertyName("iso")]
        public DateTime Iso { get; set; }

        [JsonPropertyName("epoch")]
        public long Epoch { get; set; }
    }
}