﻿using System.Text.Json.Serialization;

namespace Coinbase.Client.Models
{
    public class GetTimeResponse
    {
        [JsonPropertyName("data")]
        public Time Data { get; set; }
    }
}