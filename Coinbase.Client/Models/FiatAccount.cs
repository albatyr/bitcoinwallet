﻿using System.Text.Json.Serialization;

namespace Coinbase.Client.Models
{
    public class FiatAccount
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }
    }
}