﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Coinbase.Client.Models
{
    public class GetPaymentMethodsResponse
    {
        [JsonPropertyName("data")]
        public IList<PaymentMethod> Data { get; set; }
    }
}