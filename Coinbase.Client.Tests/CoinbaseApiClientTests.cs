using System;
using System.Threading.Tasks;
using Coinbase.Client.Web;
using Xunit;
using Xunit.Abstractions;

namespace Coinbase.Client.Tests
{
    public class CoinbaseApiClientTests
    {
        private readonly ICoinbaseApiClient _coinbaseApiClient;
        
        public CoinbaseApiClientTests(ITestOutputHelper output)
        {
            var logger = new TestOutputLogger(output);
            var webClient = new WebApiClient(new Lazy<ILog>(() => logger));

            _coinbaseApiClient = new CoinbaseApiClient(webClient);
        }
        
        
        [Fact]
        public async Task Test_GetPaymentMethods()
        {
            var paymentMethods = await _coinbaseApiClient.GetPaymentMethods();
        }
    }
}