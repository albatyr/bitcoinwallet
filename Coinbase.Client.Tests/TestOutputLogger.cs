﻿using Coinbase.Client.Web;
using Xunit.Abstractions;

namespace Coinbase.Client.Tests
{
    public class TestOutputLogger : ILog
    {
        private readonly ITestOutputHelper _output;

        public TestOutputLogger(ITestOutputHelper output)
        {
            _output = output;
        }
        
        public void Debug(string message)
        {
            _output.WriteLine(message);
        }
    }
}