﻿using System;
using System.IO;
using NBitcoin;
using static System.Console;
using static DotNetWallet.Helpers.CommandLine;

namespace DotNetWallet.Helpers
{
    public static class Assert
    {
        public static void ThatWalletExists(string walletFilePath)
        {
            if (File.Exists(walletFilePath))
            {
                Exit($"A wallet, named {walletFilePath} already exists.");
            }
        }
        public static void ThatNetworkIsSupported(Network network)
        {
            if (network != Config.Network)
            {
                WriteLine($"The wallet you want to load is on the {network} Bitcoin network.");
                WriteLine($"But your config file specifies {Config.Network} Bitcoin network.");
                Exit();
            }
        }
        public static void ThatMnemonicFormatIsCorrect(string mnemonic)
        {
            try
            {
                if (new Mnemonic(mnemonic).IsValidChecksum)
                    return;
            }
            catch (FormatException) { }
            catch (NotSupportedException) { }

            Exit("Incorrect mnemonic format.");
        }
        // Inclusive
        public static void ArgumentsLength(int length, int min, int max)
        {
            if (length < min)
            {
                Exit($"Not enough arguments are specified, minimum: {min}");
            }
            if (length > max)
            {
                Exit($"Too many arguments are specified, maximum: {max}");
            }
        }
    }
}