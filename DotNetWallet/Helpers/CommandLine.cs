﻿using System;
using System.IO;
using HBitcoin.KeyManagement;
using static System.Console;
using static DotNetWallet.Helpers.Assert;

namespace DotNetWallet.Helpers
{
    public static class CommandLine
    {
        public static string GetArgumentValue(string[] args, string argName, bool required = true)
        {
            string argValue = "";
            foreach (var arg in args)
            {
                if (arg.StartsWith($"{argName}=", StringComparison.OrdinalIgnoreCase))
                {
                    argValue = arg.Substring(arg.IndexOf("=") + 1);
                    break;
                }
            }
            if (required && argValue == "")
            {
                Exit($@"'{argName}=' is not specified.");
            }
            return argValue;
        }
        
        public static string GetWalletFilePath(string[] args)
        {
            string walletFileName = GetArgumentValue(args, "wallet-file", required: false);
            if (walletFileName == "") walletFileName = Config.DefaultWalletFileName;

            var walletDirName = "Wallets";
            Directory.CreateDirectory(walletDirName);
            return Path.Combine(walletDirName, walletFileName);
        }
        
        public static Safe DecryptWalletByAskingForPassword(string walletFilePath)
        {
            Safe safe = null;
            string pw;
            bool correctPw = false;
            WriteLine("Type your password:");
            do
            {
                pw = PasswordConsole.ReadPassword();
                try
                {
                    safe = Safe.Load(pw, walletFilePath);
                    ThatNetworkIsSupported(safe.Network);
                    correctPw = true;
                }
                catch (System.Security.SecurityException)
                {
                    WriteLine("Invalid password, try again, (or press ctrl+c to exit):");
                    correctPw = false;
                }
            } while (!correctPw);

            if (safe == null)
                throw new Exception("Wallet could not be decrypted.");
            WriteLine($"{walletFilePath} wallet is decrypted.");
            return safe;
        }
        
        public static ConsoleKey GetYesNoAnswerFromUser()
        {
            ConsoleKey response;
            do
            {
                WriteLine($"Are you sure you want to proceed? (y/n)");
                response = ReadKey(false).Key;   // true is intercept key (dont show), false is show
                if (response != ConsoleKey.Enter)
                    WriteLine();
            } while (response != ConsoleKey.Y && response != ConsoleKey.N);
            return response;
        }
        
        public static void Exit(string reason = "", ConsoleColor color = ConsoleColor.Red)
        {
            ForegroundColor = color;
            WriteLine();
            if (reason != "")
            {
                WriteLine(reason);
            }
            WriteLine("Press any key to exit...");
            ResetColor();
            ReadKey();
            Environment.Exit(0);
        }
    }
}