﻿using System;
using System.Text;

namespace DotNetWallet.Helpers
{
	public class PasswordConsole
    {
		/// <summary>
		/// Gets the console password.
		/// </summary>
		/// <returns></returns>
		internal static string ReadPassword()
		{
			StringBuilder sb = new StringBuilder();
			while (true)
			{
				ConsoleKeyInfo cki = System.Console.ReadKey(true);
				if (cki.Key == ConsoleKey.Enter)
				{
					System.Console.WriteLine();
					break;
				}

				if (cki.Key == ConsoleKey.Backspace)
				{
					if (sb.Length > 0)
					{
						System.Console.Write("\b\0\b");
						sb.Length--;
					}

					continue;
				}

				System.Console.Write('*');
				sb.Append(cki.KeyChar);
			}

			return sb.ToString();
		}
	}
}
