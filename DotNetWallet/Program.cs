﻿using DotNetWallet.Helpers;
using System;
using System.Linq;
using DotNetWallet.Commands;
using static System.Console;

namespace DotNetWallet
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //args = new string[] { "help" };
            //args = new string[] { "generate-wallet" };
            //args = new string[] { "generate-wallet", "wallet-file=test2.json" };
            ////math super cool donate beach mobile sunny web board kingdom bacon crisp
            ////no password
            //args = new string[] { "recover-wallet", "wallet-file=test5.json" };
            //args = new string[] { "show-balances", "wallet-file=test5.json" };
            //args = new string[] { "receive", "wallet-file=test4.json" };
            //args = new string[] { "show-history", "wallet-file=test.json" };
            //args = new string[] { "send", "btc=0.001", "address=mq6fK8fkFyCy9p53m4Gf4fiX2XCHvcwgi1", "wallet-file=test.json" };
            //args = new string[] { "send", "btc=all", "address=mzz63n3n89KVeHQXRqJEVsQX8MZj5zeqCw", "wallet-file=test4.json" };

            // Load config file
            // It also creates it with default settings if doesn't exist
            Config.Load();

            CommandFactory.TryCreateCommand("help", out var helpCommand);

            if (args.Length == 0)
            {
                helpCommand.Run(args);
                CommandLine.Exit(color: ConsoleColor.Green);
            }
            
            if (!CommandFactory.TryCreateCommand(args[0], out var command))
            {
                WriteLine("Wrong command is specified.");
                helpCommand.Run(args);
            }
            else
            {
                foreach (var arg in args.Skip(1))
                {
                    if (!arg.Contains('='))
                    {
                        CommandLine.Exit($"Wrong argument format specified: {arg}");
                    }
                }
                
                command.Run(args);
            }
            
            CommandLine.Exit(color: ConsoleColor.Green);
        }
    }
}
