﻿using System;
using System.Collections.Generic;
using DotNetWallet.Commands.Handlers;

namespace DotNetWallet.Commands
{
    public class CommandFactory
    {
        public static readonly IDictionary<string, Type> Commands = new Dictionary<string, Type>
        {
            {"help", typeof(HelpCommand)},
            {"generate-wallet", typeof(GenerateWalletCommand)},
            {"recover-wallet", typeof(RecoverWalletCommand)},
            {"show-balances", typeof(ShowBalancesCommand)},
            {"show-history", typeof(ShowHistoryCommand)},
            {"receive", typeof(ReceiveCommand)},
            {"send", typeof(SendCommand)}
        };
        
        public static bool TryCreateCommand(string commandName, out ICommand command)
        {
            command = null;
            
            if (!Commands.ContainsKey(commandName))
                return false;

            var commandType = Commands[commandName];

            command = (ICommand) Activator.CreateInstance(commandType);
            
            return true;
        }
    }
}