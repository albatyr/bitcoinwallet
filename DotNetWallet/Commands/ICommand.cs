﻿namespace DotNetWallet.Commands
{
    public interface ICommand
    {
        public void Run(string[] args);
    }
}