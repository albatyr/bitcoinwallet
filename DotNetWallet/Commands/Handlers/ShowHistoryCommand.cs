﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotNetWallet.Helpers;
using HBitcoin.KeyManagement;
using NBitcoin;
using QBitNinja.Client.Models;
using static System.Console;
using static DotNetWallet.QBitNinjaJutsus.QBitNinjaJutsus;

namespace DotNetWallet.Commands.Handlers
{
    public class ShowHistoryCommand : ICommand
    {
        public void Run(string[] args)
        {
            Assert.ArgumentsLength(args.Length, 1, 2);
            var walletFilePath = CommandLine.GetWalletFilePath(args);
            Safe safe = CommandLine.DecryptWalletByAskingForPassword(walletFilePath);

            if (Config.ConnectionType == ConnectionType.Http)
            {
                // 0. Query all operations, grouped our used safe addresses
                Dictionary<BitcoinAddress, List<BalanceOperation>> operationsPerAddresses =
                    QueryOperationsPerSafeAddresses(safe);

                WriteLine();
                WriteLine("---------------------------------------------------------------------------");
                WriteLine("Date\t\t\tAmount\t\tConfirmed\tTransaction Id");
                WriteLine("---------------------------------------------------------------------------");

                Dictionary<uint256, List<BalanceOperation>> operationsPerTransactions =
                    GetOperationsPerTransactions(operationsPerAddresses);

                // 3. Create history records from the transactions
                // History records is arbitrary data we want to show to the user
                var txHistoryRecords = new List<Tuple<DateTimeOffset, Money, int, uint256>>();
                foreach (var elem in operationsPerTransactions)
                {
                    var amount = Money.Zero;
                    foreach (var op in elem.Value)
                        amount += op.Amount;
                    var firstOp = elem.Value.First();

                    txHistoryRecords
                        .Add(new Tuple<DateTimeOffset, Money, int, uint256>(
                            firstOp.FirstSeen,
                            amount,
                            firstOp.Confirmations,
                            elem.Key));
                }

                // 4. Order the records by confirmations and time (Simply time does not work, because of a QBitNinja bug)
                var orderedTxHistoryRecords = txHistoryRecords
                    .OrderByDescending(x => x.Item3) // Confirmations
                    .ThenBy(x => x.Item1); // FirstSeen
                foreach (var record in orderedTxHistoryRecords)
                {
                    // Item2 is the Amount
                    if (record.Item2 > 0) ForegroundColor = ConsoleColor.Green;
                    else if (record.Item2 < 0) ForegroundColor = ConsoleColor.Red;
                    WriteLine($"{record.Item1.DateTime}\t{record.Item2}\t{record.Item3 > 0}\t\t{record.Item4}");
                    ResetColor();
                }
            }
            else if (Config.ConnectionType == ConnectionType.FullNode)
            {
                throw new NotImplementedException();
            }
            else
            {
                CommandLine.Exit("Invalid connection type.");
            }
        }
    }
}