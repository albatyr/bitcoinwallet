﻿using DotNetWallet.Helpers;
using HBitcoin.KeyManagement;
using NBitcoin;
using static System.Console;

namespace DotNetWallet.Commands.Handlers
{
    public class GenerateWalletCommand : ICommand
    {
        public void Run(string[] args)
        {
            Assert.ArgumentsLength(args.Length, 1, 2);
            var walletFilePath = CommandLine.GetWalletFilePath(args);
            Assert.ThatWalletExists(walletFilePath);

            var password = WaitForNewPassword();
            
            // 3. Create wallet
            Safe safe = Safe.Create(out var mnemonic, password, walletFilePath, Config.Network);
            // If no exception thrown the wallet is successfully created.
            WriteLine();
            WriteLine("Wallet is successfully created.");
            WriteLine($"Wallet file: {walletFilePath}");

            // 4. Display mnemonic
            WriteLine();
            WriteLine("Write down the following mnemonic words.");
            WriteLine("With the mnemonic words AND your password you can recover this wallet by using the recover-wallet command.");
            WriteLine();
            WriteLine("-------");
            WriteLine(mnemonic);
            WriteLine("-------");
        }

        private static string WaitForNewPassword()
        {
            string pw;
            string pwConf;
            do
            {
                // 1. Get password from user
                WriteLine("Choose a password:");
                pw = PasswordConsole.ReadPassword();
                // 2. Get password confirmation from user
                WriteLine("Confirm password:");
                pwConf = PasswordConsole.ReadPassword();

                if (pw != pwConf) WriteLine("Passwords do not match. Try again!");
            } while (pw != pwConf);

            return pw;
        }
    }
}