﻿using DotNetWallet.Helpers;
using HBitcoin.KeyManagement;
using NBitcoin;
using static System.Console;

namespace DotNetWallet.Commands.Handlers
{
    public class RecoverWalletCommand : ICommand
    {
        public void Run(string[] args)
        {
            Assert.ArgumentsLength(args.Length, 1, 2);
            
            var walletFilePath = CommandLine.GetWalletFilePath(args);
            
            Assert.ThatWalletExists(walletFilePath);

            WriteLine($"Your software is configured using the Bitcoin {Config.Network} network.");
            WriteLine("Provide your mnemonic words, separated by spaces:");
            var mnemonicString = ReadLine();
            Assert.ThatMnemonicFormatIsCorrect(mnemonicString);
            var mnemonic = new Mnemonic(mnemonicString);

            WriteLine(
                "Provide your password. Please note the wallet cannot check if your password is correct or not. If you provide a wrong password a wallet will be recovered with your provided mnemonic AND password pair:");
            var password = PasswordConsole.ReadPassword();

            Safe safe = Safe.Recover(mnemonic, password, walletFilePath, Config.Network);
            // If no exception thrown the wallet is successfully recovered.
            WriteLine();
            WriteLine("Wallet is successfully recovered.");
            WriteLine($"Wallet file: {walletFilePath}");
        }
    }
}