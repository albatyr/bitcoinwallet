﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading;
using DotNetWallet.Helpers;
using HBitcoin.KeyManagement;
using NBitcoin;
using Newtonsoft.Json.Linq;
using QBitNinja.Client;
using QBitNinja.Client.Models;
using static System.Console;
using static DotNetWallet.Helpers.CommandLine;
using static DotNetWallet.QBitNinjaJutsus.QBitNinjaJutsus;

namespace DotNetWallet.Commands.Handlers
{
    public class SendCommand : ICommand
    {
        public void Run(string[] args)
        {
            Assert.ArgumentsLength(args.Length, 3, 4);
            var walletFilePath = GetWalletFilePath(args);
            BitcoinAddress addressToSend;
            try
            {
                string address = GetArgumentValue(args, argName: "address", required: true);
                addressToSend = BitcoinAddress.Create(address, Config.Network);
            }
            catch (Exception ex)
            {
                Exit(ex.ToString());
                throw ex;
            }

            Safe safe = DecryptWalletByAskingForPassword(walletFilePath);

            if (Config.ConnectionType == ConnectionType.Http)
            {
                Dictionary<BitcoinAddress, List<BalanceOperation>> operationsPerAddresses =
                    QueryOperationsPerSafeAddresses(safe, 7);

                // 1. Gather all the not empty private keys
                WriteLine("Finding not empty private keys...");
                var operationsPerNotEmptyPrivateKeys = new Dictionary<BitcoinExtKey, List<BalanceOperation>>();
                foreach (var elem in operationsPerAddresses)
                {
                    var balance = Money.Zero;
                    foreach (var op in elem.Value) balance += op.Amount;
                    if (balance > Money.Zero)
                    {
                        var secret = safe.FindPrivateKey(elem.Key);
                        operationsPerNotEmptyPrivateKeys.Add(secret, elem.Value);
                    }
                }

                // 2. Get the script pubkey of the change.
                WriteLine("Select change address...");
                Script changeScriptPubKey = null;
                Dictionary<BitcoinAddress, List<BalanceOperation>> operationsPerChangeAddresses =
                    QueryOperationsPerSafeAddresses(safe, minUnusedKeys: 1, hdPathType: HdPathType.Change);
                foreach (var elem in operationsPerChangeAddresses)
                {
                    if (elem.Value.Count == 0)
                        changeScriptPubKey = safe.FindPrivateKey(elem.Key).ScriptPubKey;
                }

                if (changeScriptPubKey == null)
                    throw new ArgumentNullException();

                // 3. Gather coins can be spend
                WriteLine("Gathering unspent coins...");
                Dictionary<Coin, bool> unspentCoins = GetUnspentCoins(operationsPerNotEmptyPrivateKeys.Keys);

                // 4. Get the fee
                WriteLine("Calculating transaction fee...");
                Money fee;
                try
                {
                    var txSizeInBytes = 250;
                    using var client = new HttpClient();
                    const string request = @"https://bitcoinfees.earn.com/api/v1/fees/recommended";
                    var result = client.GetAsync(request, HttpCompletionOption.ResponseContentRead).Result;
                    var json = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    var fastestSatoshiPerByteFee = json.Value<decimal>("fastestFee");
                    fee = new Money(fastestSatoshiPerByteFee * txSizeInBytes, MoneyUnit.Satoshi);
                }
                catch
                {
                    Exit("Couldn't calculate transaction fee, try it again later.");
                    throw new Exception("Can't get tx fee");
                }

                WriteLine($"Fee: {fee.ToDecimal(MoneyUnit.BTC).ToString("0.#############################")}btc");

                // 5. How much money we can spend?
                Money availableAmount = unspentCoins.Sum(x => x.Key.Amount);

                // 6. How much to spend?
                Money amountToSend = null;
                string amountString = GetArgumentValue(args, argName: "btc", required: true);
                if (string.Equals(amountString, "all", StringComparison.OrdinalIgnoreCase))
                {
                    amountToSend = availableAmount;
                    amountToSend -= fee;
                }
                else
                {
                    amountToSend = ParseBtcString(amountString);
                }

                // 7. Do some checks
                if (amountToSend < Money.Zero || availableAmount < amountToSend + fee)
                    Exit("Not enough coins.");

                decimal feePc =
                    Math.Round((100 * fee.ToDecimal(MoneyUnit.BTC)) / amountToSend.ToDecimal(MoneyUnit.BTC));
                if (feePc > 1)
                {
                    WriteLine();
                    WriteLine($"The transaction fee is {feePc.ToString("0.#")}% of your transaction amount.");
                    WriteLine(
                        $"Sending:\t {amountToSend.ToDecimal(MoneyUnit.BTC).ToString("0.#############################")}btc");
                    WriteLine(
                        $"Fee:\t\t {fee.ToDecimal(MoneyUnit.BTC).ToString("0.#############################")}btc");
                    ConsoleKey response = GetYesNoAnswerFromUser();
                    if (response == ConsoleKey.N)
                    {
                        Exit("User interruption.");
                    }
                }

                var totalOutAmount = amountToSend + fee;

                // 8. Select coins
                WriteLine("Selecting coins...");
                var coinsToSpend = new HashSet<Coin>();
                var unspentConfirmedCoins = new List<Coin>();
                var unspentUnconfirmedCoins = new List<Coin>();
                foreach (var elem in unspentCoins)
                    if (elem.Value) unspentConfirmedCoins.Add(elem.Key);
                    else unspentUnconfirmedCoins.Add(elem.Key);

                bool haveEnough = SelectCoins(ref coinsToSpend, totalOutAmount, unspentConfirmedCoins);
                if (!haveEnough)
                    haveEnough = SelectCoins(ref coinsToSpend, totalOutAmount, unspentUnconfirmedCoins);
                if (!haveEnough)
                    throw new Exception("Not enough funds.");

                // 9. Get signing keys
                var signingKeys = new HashSet<ISecret>();
                foreach (var coin in coinsToSpend)
                {
                    foreach (var elem in operationsPerNotEmptyPrivateKeys)
                    {
                        if (elem.Key.ScriptPubKey == coin.ScriptPubKey)
                            signingKeys.Add(elem.Key);
                    }
                }

                // 10. Build the transaction
                WriteLine("Signing transaction...");
                var builder = new TransactionBuilder();
                var tx = builder
                    .AddCoins(coinsToSpend)
                    .AddKeys(signingKeys.ToArray())
                    .Send(addressToSend, amountToSend)
                    .SetChange(changeScriptPubKey)
                    .SendFees(fee)
                    .BuildTransaction(true);

                if (!builder.Verify(tx))
                    Exit("Couldn't build the transaction.");

                WriteLine($"Transaction Id: {tx.GetHash()}");

                var qBitClient = new QBitNinjaClient(Config.Network);

                // QBit's success response is buggy so let's check manually, too		
                BroadcastResponse broadcastResponse;
                var success = false;
                var tried = 0;
                var maxTry = 7;
                do
                {
                    tried++;
                    WriteLine($"Try broadcasting transaction... ({tried})");
                    broadcastResponse = qBitClient.Broadcast(tx).Result;
                    var getTxResp = qBitClient.GetTransaction(tx.GetHash()).Result;
                    if (getTxResp == null)
                    {
                        Thread.Sleep(3000);
                        continue;
                    }
                    else
                    {
                        success = true;
                        break;
                    }
                } while (tried <= maxTry);

                if (!success)
                {
                    if (broadcastResponse.Error != null)
                    {
                        WriteLine(
                            $"Error code: {broadcastResponse.Error.ErrorCode} Reason: {broadcastResponse.Error.Reason}");
                    }

                    Exit(
                        $"The transaction might not have been successfully broadcasted. Please check the Transaction ID in a block explorer.",
                        ConsoleColor.Blue);
                }

                Exit("Transaction is successfully propagated on the network.", ConsoleColor.Green);
            }
            else if (Config.ConnectionType == ConnectionType.FullNode)
            {
                throw new NotImplementedException();
            }
            else
            {
                Exit("Invalid connection type.");
            }
        }
        
        private static Money ParseBtcString(string value)
        {
            decimal amount;
            if (!decimal.TryParse(
                value.Replace(',', '.'),
                NumberStyles.Any,
                CultureInfo.InvariantCulture,
                out amount))
            {
                Exit("Wrong btc amount format.");
            }


            return new Money(amount, MoneyUnit.BTC);
        }
    }
}