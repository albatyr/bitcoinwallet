﻿using DotNetWallet.Helpers;
using Console = System.Console;

namespace DotNetWallet.Commands.Handlers
{
    public class HelpCommand : ICommand
    {
        public void Run(string[] args)
        {
            Assert.ArgumentsLength(args.Length, 1, 1);
            
            Console.WriteLine("Possible commands are:");
            
            foreach (var cmd in CommandFactory.Commands.Keys) 
                Console.WriteLine($"\t{cmd}");
        }
    }
}