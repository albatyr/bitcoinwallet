﻿using System;
using System.Collections.Generic;
using DotNetWallet.Helpers;
using HBitcoin.KeyManagement;
using NBitcoin;
using QBitNinja.Client.Models;
using static System.Console;
using static DotNetWallet.QBitNinjaJutsus.QBitNinjaJutsus;

namespace DotNetWallet.Commands.Handlers
{
    public class ReceiveCommand : ICommand
    {
        public void Run(string[] args)
        {
            Assert.ArgumentsLength(args.Length, 1, 2);
            var walletFilePath = CommandLine.GetWalletFilePath(args);
            Safe safe = CommandLine.DecryptWalletByAskingForPassword(walletFilePath);

            if (Config.ConnectionType == ConnectionType.Http)
            {
                Dictionary<BitcoinAddress, List<BalanceOperation>> operationsPerReceiveAddresses = 
                    QueryOperationsPerSafeAddresses(safe, 7, HdPathType.Receive);

                WriteLine("---------------------------------------------------------------------------");
                WriteLine("Unused Receive Addresses");
                WriteLine("---------------------------------------------------------------------------");
                foreach (var elem in operationsPerReceiveAddresses)
                    if (elem.Value.Count == 0)
                        WriteLine($"{elem.Key.ToString()}");
            }
            else if (Config.ConnectionType == ConnectionType.FullNode)
            {
                throw new NotImplementedException();
            }
            else
            {
                CommandLine.Exit("Invalid connection type.");
            }
        }
    }
}